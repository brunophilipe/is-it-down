//
//  BPNewServerTableViewController.m
//  Is It Down
//
//  Created by Bruno Philipe on 9/19/14.
//  Copyright (c) 2014 Bruno Philipe. All rights reserved.
//

#import "BPNewServerViewController.h"
#import "BPAppDelegate.h"
#import "NSMutableArray+MoveObject.h"
#import "BPPortViewController.h"
#import "DAKeyboardControl.h"
#import "UIView+DismissKeyboard.h"

#import "globalstrings.h"

#define UIColorFromHEX(c) [UIColor colorWithRed:((c>>24)&0xFF)/255.0 green:((c>>16)&0xFF)/255.0 blue:((c>>8)&0xFF)/255.0  alpha:((c)&0xFF)/255.0]

@interface BPNewServerViewController () <UITextFieldDelegate>

@property (strong) UITextField *textField_serverAddress;
@property (strong) UITextField *textField_serverDisplayName;
@property (strong) UIActivityIndicatorView *activityIndicator;
@property (strong) UIBarButtonItem *doneButtonItem;
@property (strong) NSUserDefaults *userDefaults;
@property (strong) NSMutableDictionary *serverInfo;
@property (strong) NSMutableArray *serverPorts;
@property (strong) UIToolbar *inputAccessoryToolbar;

@property NSInteger lastSelectedRow;

@property (nonatomic) BOOL isHostInvalid;

//

@property (strong) NSDictionary *editedPort;

@end

@implementation BPNewServerViewController

+ (NSMutableArray*)defaultPortsArray
{
	return [@[@{kBPServerPortNameKey: @"http", kBPServerPortNumberKey: @80}] mutableCopy];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	[self.view addKeyboardPanningWithActionHandler:nil];

	[self setDoneButtonItem:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(didTapDone:)]];

	[self setToolbarItems:@[self.editButtonItem,
							[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
							self.doneButtonItem]];

	self.userDefaults = [BPAppDelegateRef sharedUserDefaults];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];

	NSArray *servers = [self.userDefaults arrayForKey:kBPServersKey];

	if (!_serverPorts || !_serverInfo) {
		if (servers && self.editingServerIndex && self.editingServerIndex.integerValue >= 0 && self.editingServerIndex.integerValue < servers.count) {
			_serverInfo = [[servers objectAtIndex:self.editingServerIndex.integerValue] mutableCopy];
			if ([_serverInfo isKindOfClass:[NSDictionary class]])
			{
				[self setServerPorts:[[_serverInfo objectForKey:kBPServerPortsKey] mutableCopy]];
				if (!_serverPorts)
					_serverPorts = [self.class defaultPortsArray];
			}
		} else {
			_serverInfo = [NSMutableDictionary new];
			_serverPorts = [self.class defaultPortsArray];
		}
	}

	if (_editedPort) {
		if (_lastSelectedRow >= 0) {
			[_serverPorts replaceObjectAtIndex:_lastSelectedRow withObject:_editedPort];
		} else {
			[_serverPorts addObject:_editedPort];
		}
	}

	_editedPort = nil;
	_lastSelectedRow = -1;

	self.inputAccessoryToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44.f)];
	[self.inputAccessoryToolbar setTranslucent:YES];
	[self.inputAccessoryToolbar setItems:@[[[UIBarButtonItem alloc] initWithTitle:@"Hide" style:UIBarButtonItemStylePlain target:self action:@selector(dismissKeyboard)],
								  [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
								  [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(didTapDone:)]]];

	[self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dismissKeyboard
{
	[self.view findAndResignFirstResponder];
}

- (void)storeServerURL
{
	NSString *serverInput = [[self textField_serverAddress] text];
	NSURL *url = [NSURL URLWithString:serverInput];

	if ([serverInput length] > 0 && [url host])
	{
		[self setIsHostInvalid:NO];

		[_serverInfo setObject:serverInput forKey:kBPServerURLKey];

		if ([[self.textField_serverDisplayName text] length] == 0)
		{
			NSString *displayName = [url host];

			[self.textField_serverDisplayName setText:displayName];
			[_serverInfo setObject:displayName forKey:kBPServerDisplayNameKey];
		}
	}
	else
	{
		// Host input is not valid
		[self setIsHostInvalid:YES];
	}
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

- (void)removeDeprecatedKeysFromDictionary:(NSMutableDictionary*)dictionary
{
	[dictionary removeObjectForKey:kBPServerPortReachabilityKey];
	[dictionary removeObjectForKey:kBPServerLastDowntimeKey];
}

#pragma GCC diagnostic pop

- (void)didTapDone:(id)sender
{
	if ([[self textField_serverAddress] isFirstResponder])
	{
		[[self textField_serverAddress] resignFirstResponder];
	}
	else
	{
		[self storeServerURL];
	}


	if ([self isHostInvalid])
	{
		return;
	}

	if ([[self textField_serverDisplayName] isFirstResponder])
	{
		[[self textField_serverDisplayName] resignFirstResponder];
	}

	NSArray *servers = [[self.userDefaults arrayForKey:kBPServersKey] mutableCopy];

	if (self.editingServerIndex)
	{
		assert(servers);

		NSMutableArray *ports = [NSMutableArray arrayWithCapacity:_serverPorts.count];

		for (NSDictionary *port in _serverPorts)
		{
			NSMutableDictionary *portMutable = [port mutableCopy];
			[portMutable removeObjectForKey:kBPServerPortReachabilityKey];
			[portMutable removeObjectForKey:kBPServerPortLastDowntimeKey];
			[ports addObject:[portMutable copy]];
		}

		NSMutableDictionary *serverInfo = [[servers objectAtIndex:self.editingServerIndex.integerValue] mutableCopy];
		[serverInfo setObject:[_serverInfo objectForKey:kBPServerDisplayNameKey] forKey:kBPServerDisplayNameKey];
		[serverInfo setObject:[_serverInfo objectForKey:kBPServerURLKey] forKey:kBPServerURLKey];
		[serverInfo setObject:[ports copy] forKey:kBPServerPortsKey];
		[self removeDeprecatedKeysFromDictionary:serverInfo];

		[(NSMutableArray*)servers replaceObjectAtIndex:self.editingServerIndex.integerValue withObject:serverInfo];
	}
	else
	{
		NSDictionary *serverInfo = @{kBPServerDisplayNameKey: [_serverInfo objectForKey:kBPServerDisplayNameKey],
									 kBPServerURLKey: [_serverInfo objectForKey:kBPServerURLKey],
									 kBPServerPortsKey: _serverPorts};

		if (!servers)
			servers = @[serverInfo];
		else
			[(NSMutableArray*)servers addObject:serverInfo];
	}

	[self.userDefaults setObject:servers forKey:kBPServersKey];
	[self.userDefaults synchronize];

	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Accessors

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
	[super setEditing:editing animated:animated];

	[self.doneButtonItem setEnabled:!editing];
	[self.textField_serverDisplayName setEnabled:!editing];
	[self.textField_serverAddress setEnabled:!editing];
}

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (section == 1)
		return _serverPorts.count;
	else
		return 1;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *identifier_port = @"cell_port";
	static NSString *identifier_url = @"cell_url";
	static NSString *identifier_name = @"cell_display_name";

	UITableViewCell *cell = nil;

	switch (indexPath.section)
	{
		case 0:
		{
			NSString *oldTextFieldContents = [[self textField_serverAddress] text];

			cell = [tableView dequeueReusableCellWithIdentifier:identifier_url forIndexPath:indexPath];
			self.textField_serverAddress = (UITextField*)[cell viewWithTag:1];
			self.textField_serverAddress.inputAccessoryView = self.inputAccessoryToolbar;

			if ([self isHostInvalid])
			{
				UIColor *backgroundColor = UIColorFromHEX(0xFED3DAFF);

				[[self textField_serverAddress] setTextColor:UIColorFromHEX(0xFF1F3FFF)];
				[[self textField_serverAddress] setBackgroundColor:backgroundColor];

				[cell setBackgroundColor:backgroundColor];
			}
			else if ([_serverInfo objectForKey:kBPServerURLKey] != nil)
			{
				// We shouldn't update the text field contents in this case.
				[self.textField_serverAddress setText:[_serverInfo objectForKey:kBPServerURLKey]];
			}
			else if (oldTextFieldContents != nil)
			{
				[self.textField_serverAddress setText:oldTextFieldContents];
			}
		}
			break;

		case 1:
		{
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:identifier_port];
			[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];

			NSDictionary *portInfo = [_serverPorts objectAtIndex:indexPath.row];
			[cell.textLabel		  setText:[portInfo objectForKey:kBPServerPortNameKey]];
			[cell.detailTextLabel setText:[NSString stringWithFormat:@"%ld", (long)[[portInfo objectForKey:kBPServerPortNumberKey] integerValue]]];
		}
			break;

		case 2:
		{
			cell = [tableView dequeueReusableCellWithIdentifier:identifier_name forIndexPath:indexPath];
			self.textField_serverDisplayName = (UITextField*)[cell viewWithTag:1];
			self.textField_serverDisplayName.inputAccessoryView = self.inputAccessoryToolbar;
			[self.textField_serverDisplayName setText:[_serverInfo objectForKey:kBPServerDisplayNameKey]];
		}
			break;
	}

	return cell;
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	switch (section) {
		case 0: return @"Server Address"; break;
		case 1: return @"Server Ports"; break;
		case 2: return @"Server Display Name"; break;
	}

	return @"";
}

- (NSString*)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
	if (section == 0)
	{
		if ([self isHostInvalid])
		{
			return @"This doesn't seem to be a URL with a valid host. Try apple.com for example.";
		}
		else
		{
			return @"Please notice only the host part of the URL (domain) will be used to check for reachability.";
		}
	}
	return nil;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
	return (indexPath.section == 1 && _serverPorts.count > 1);
}

- (NSIndexPath *)tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath
{
	if (proposedDestinationIndexPath.section > sourceIndexPath.section) {
		return [NSIndexPath indexPathForRow:_serverPorts.count-1 inSection:1];
	} else if (proposedDestinationIndexPath.section < sourceIndexPath.section) {
		return [NSIndexPath indexPathForRow:0 inSection:1];
	} else {
		return proposedDestinationIndexPath;
	}
}

#pragma mark - Getters & Setters

- (void)setIsHostInvalid:(BOOL)isHostInvalid
{
	_isHostInvalid = isHostInvalid;

	[[self tableView] reloadSections:[NSIndexSet indexSetWithIndex:0]
					withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Table View Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 44.0;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (editingStyle == UITableViewCellEditingStyleDelete) {
		[tableView beginUpdates];
		[_serverPorts removeObjectAtIndex:indexPath.row];
		[tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
		[tableView endUpdates];
	}
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
	[_serverPorts moveObjectFromIndex:sourceIndexPath.row toIndex:destinationIndexPath.row];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];

	if (indexPath.section == 1) {
		[self setLastSelectedRow:indexPath.row];
		[self performSegueWithIdentifier:@"edit_port" sender:tableView];
	}
}

#pragma mark - Text Field Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	if (textField == self.textField_serverAddress)
	{
		[self.textField_serverDisplayName becomeFirstResponder];
	}
	else
	{
		[self.textField_serverDisplayName resignFirstResponder];
		[self didTapDone:textField];
	}
	return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
	if (textField == self.textField_serverAddress)
	{
		if ([[textField text] length] == 0)
		{
			[textField setText:@"http://"];
		}
	}
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
	if (textField == self.textField_serverAddress)
	{
		[self storeServerURL];
	}
	else
	{
		if ([[textField text] length] > 0)
		{
			[_serverInfo setObject:[textField text] forKey:kBPServerDisplayNameKey];
		}
	}
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"edit_port"] && _lastSelectedRow >= 0) {
		[(BPPortViewController*)segue.destinationViewController setEditingPort:[_serverPorts objectAtIndex:_lastSelectedRow]];
	} else {
		[(BPPortViewController*)segue.destinationViewController setEditingPort:nil];
	}

	[(BPPortViewController*)segue.destinationViewController setCallerViewController:self];
}

@end

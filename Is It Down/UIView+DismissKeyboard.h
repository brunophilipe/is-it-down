//
//  UIViewController_DismissKeyboard.h
//  Is It Down
//
//  Created by Bruno Philipe on 11/5/14.
//  Copyright (c) 2014 Bruno Philipe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (DismissKeyboard)

- (BOOL)findAndResignFirstResponder;

@end

//
//  globalstrings.h
//  Is It Down
//
//  Created by Bruno Philipe on 9/19/14.
//  Copyright (c) 2014 Bruno Philipe. All rights reserved.
//

#ifndef __Is_It_Down__globalstrings__
#define __Is_It_Down__globalstrings__

#import <Foundation/Foundation.h>

extern NSString *const kBPSharedSuiteName;

extern NSString *const kBPServersKey;

extern NSString *const kBPServerURLKey;
extern NSString *const kBPServerPortsKey;
extern NSString *const kBPServerPortNameKey;
extern NSString *const kBPServerPortNumberKey;
extern NSString *const kBPServerDisplayNameKey;
extern NSString *const kBPServerReachabilityKey __deprecated;
extern NSString *const kBPServerLastDowntimeKey __deprecated;
extern NSString *const kBPServerPortReachabilityKey;
extern NSString *const kBPServerPortLastDowntimeKey;

extern NSString *const kBPPortDisplayModeKey;
#define kBPPortDisplayNumber 0
#define kBPPortDisplayName 1

extern NSString *const kBPShowsReloadButtonKey;

extern NSString *const kBPLastCheckTimestampKey;

#endif /* defined(__Is_It_Down__globalstrings__) */

//
//  BPNewServerTableViewController.h
//  Is It Down
//
//  Created by Bruno Philipe on 9/19/14.
//  Copyright (c) 2014 Bruno Philipe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BPNewServerViewController : UITableViewController

@property (strong) NSNumber *editingServerIndex;

@end

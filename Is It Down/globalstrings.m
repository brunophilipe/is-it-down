//
//  globalstrings.c
//  Is It Down
//
//  Created by Bruno Philipe on 9/19/14.
//  Copyright (c) 2014 Bruno Philipe. All rights reserved.
//

#include "globalstrings.h"

NSString *const kBPSharedSuiteName = @"group.com.brunophilipe.Is-It-Down";

NSString *const kBPServersKey = @"BPServersKey";

NSString *const kBPServerURLKey = @"BPServerURLKey";
NSString *const kBPServerPortsKey = @"BPServerPortsKey";
NSString *const kBPServerPortNameKey = @"BPServerPortNameKey";
NSString *const kBPServerPortNumberKey = @"BPServerPortNumberKey";
NSString *const kBPServerDisplayNameKey = @"BPServerDisplayNameKey";
NSString *const kBPServerReachabilityKey __deprecated = @"BPServerReachabilityKey";
NSString *const kBPServerLastDowntimeKey __deprecated = @"BPServerLastDowntimeKey";
NSString *const kBPServerPortReachabilityKey = @"BPServerPortReachabilityKey";
NSString *const kBPServerPortLastDowntimeKey = @"BPServerPortLastDowntimeKey";

NSString *const kBPPortDisplayModeKey = @"BPPortDisplayModeKey";
NSString *const kBPShowsReloadButtonKey = @"BPShowsReloadButtonKey";

NSString *const kBPLastCheckTimestampKey = @"BPLastCheckTimestampKey";
//
//  BPPortViewController.h
//  Is It Down
//
//  Created by Bruno Philipe on 10/3/14.
//  Copyright (c) 2014 Bruno Philipe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BPNewServerViewController.h"

@interface BPPortViewController : UITableViewController

@property (weak) BPNewServerViewController *callerViewController;

@property (strong) NSDictionary *editingPort;

@end

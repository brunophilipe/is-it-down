//
//  AppDelegate.h
//  Is It Down
//
//  Created by Bruno Philipe on 9/19/14.
//  Copyright (c) 2014 Bruno Philipe. All rights reserved.
//

#import <UIKit/UIKit.h>

#define BPAppDelegateRef ((BPAppDelegate*)[[UIApplication sharedApplication] delegate])

@interface BPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly) NSUserDefaults *sharedUserDefaults;

@end


//
//  BPServersTableViewController.m
//  Is It Down
//
//  Created by Bruno Philipe on 9/19/14.
//  Copyright (c) 2014 Bruno Philipe. All rights reserved.
//

#import "BPServersViewController.h"
#import "BPNewServerViewController.h"
#import "BPAppDelegate.h"
#import "NSMutableArray+MoveObject.h"
#import "DAKeyboardControl.h"

#import "globalstrings.h"

@interface BPServersViewController ()

@property (strong) NSMutableArray *servers;
@property (strong) NSUserDefaults *userDefaults;

@property BOOL noServers;

@end

@implementation BPServersViewController

- (void)viewDidLoad {
    [super viewDidLoad];

	self.userDefaults = [BPAppDelegateRef sharedUserDefaults];

	self.navigationItem.leftBarButtonItem = self.editButtonItem;
	
	[self.view addKeyboardPanningWithActionHandler:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];

	self.servers = [[self.userDefaults arrayForKey:kBPServersKey] mutableCopy];

	static BOOL firstTime = YES;
	if (firstTime) {
		firstTime = NO;
	} else {
		[self.tableView reloadData];
	}
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateServersInDefaults
{
	[self.userDefaults setObject:self.servers forKey:kBPServersKey];
	[self.userDefaults synchronize];
}

- (NSString*)portsSummaryStringForServer:(NSDictionary*)server
{
	NSArray *_ports = [server objectForKey:kBPServerPortsKey];

	if (_ports && _ports.count > 0) {
		NSMutableString *summaryString = [[NSMutableString alloc] initWithString:@"Ports: "];

		for (NSDictionary *port in _ports) {
			[summaryString appendFormat:@"%d, ", [[port objectForKey:kBPServerPortNumberKey] intValue]];
		}

		[summaryString deleteCharactersInRange:NSMakeRange(summaryString.length - 2, 2)];

		return [summaryString copy];
	} else {
		return @"Ports: 80";
	}
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	NSInteger serverCount = self.servers.count;
	if (serverCount == 0) {
		serverCount = 1;
		self.noServers = YES;
	} else {
		self.noServers = NO;
	}
    return serverCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	NSString *const identifierServer = @"cell_server";
	NSString *const identifierEmpty = @"cell_empty";
	NSString *identifier;

	if (self.noServers) {
		identifier = identifierEmpty;
	} else {
		identifier = identifierServer;
	}

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    
    // Configure the cell...
	if (!self.noServers) {
		NSDictionary *serverInfo = [self.servers objectAtIndex:indexPath.row];
		if ([serverInfo isKindOfClass:[NSDictionary class]])
		{
			[cell.textLabel setText:[serverInfo objectForKey:kBPServerDisplayNameKey]];
			[cell.detailTextLabel setText:[self portsSummaryStringForServer:serverInfo]];
		}
	}
    
    return cell;
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (editingStyle == UITableViewCellEditingStyleDelete)
	{
		[tableView beginUpdates];
		[self.servers removeObjectAtIndex:indexPath.row];

		if ([self.servers count] == 0)
		{
			// A "no servers" cell will be shown instead
			[tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
		}
		else
		{
			[tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
		}

		[tableView endUpdates];

		[self updateServersInDefaults];
	}
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
	[self.servers moveObjectFromIndex:sourceIndexPath.row toIndex:destinationIndexPath.row];
	[self updateServersInDefaults];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 44.f;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	if (	[segue.identifier isEqualToString:@"edit"] &&
			[segue.destinationViewController respondsToSelector:@selector(setEditingServerIndex:)])
	{
		NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
		[segue.destinationViewController setEditingServerIndex:@(indexPath.row)];
	}
}

@end

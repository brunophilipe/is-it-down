//
//  UIViewController_DismissKeyboard.h
//  Is It Down
//
//  Created by Bruno Philipe on 11/5/14.
//  Copyright (c) 2014 Bruno Philipe. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIView+DismissKeyboard.h"

@implementation UIView (DismissKeyboard)

- (BOOL)findAndResignFirstResponder
{
	if (self.isFirstResponder) {
		[self resignFirstResponder];
		return YES;
	}
	for (UIView *subView in self.subviews) {
		if ([subView findAndResignFirstResponder])
			return YES;
	}
	return NO;
}

@end

//
//  BPPortViewController.m
//  Is It Down
//
//  Created by Bruno Philipe on 10/3/14.
//  Copyright (c) 2014 Bruno Philipe. All rights reserved.
//

#import "BPPortViewController.h"
#import "BPNewServerViewController.h"
#import "DAKeyboardControl.h"
#import "UIView+DismissKeyboard.h"

#import "globalstrings.h"

@interface BPNewServerViewController ()

- (void)setEditedPort:(NSDictionary*)dictionary;

@end

@interface BPPortViewController () <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *textField_portNumber;
@property (strong, nonatomic) IBOutlet UITextField *textField_portName;

@property (strong) NSDictionary *wellKnownPorts;

@end

@implementation BPPortViewController

- (void)viewDidLoad {
    [super viewDidLoad];

	[self.view addKeyboardPanningWithActionHandler:nil];

	NSError *error;
	self.wellKnownPorts = [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfURL:[[NSBundle mainBundle] URLForResource:@"ports" withExtension:@"json"]] options:NSJSONReadingAllowFragments error:&error];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];

	if (self.editingPort) {
		[self.textField_portName setText:[self.editingPort objectForKey:kBPServerPortNameKey]];
		[self.textField_portNumber setText:[NSString stringWithFormat:@"%ld", (long)[[self.editingPort objectForKey:kBPServerPortNumberKey] integerValue]]];
	}
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dismissKeyboard
{
	[self.view findAndResignFirstResponder];
}

#pragma mark - text Field Delegate

- (void)textFieldDidEndEditing:(UITextField *)textField
{
	if (textField == self.textField_portNumber) {
		NSInteger number = [self.textField_portNumber.text integerValue];
		number = MAX(1, MIN(65535, number));
		[self.textField_portNumber setText:[NSString stringWithFormat:@"%ld", (long)number]];

		NSString *portName = [self.wellKnownPorts objectForKey:self.textField_portNumber.text];
		if (portName) {
			[self.textField_portName setText:portName];
		}
	}
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	if (textField == self.textField_portNumber) {
		[self.textField_portName becomeFirstResponder];
	} else {
		[self.textField_portName resignFirstResponder];
		[self didTapDone:textField];
	}
	return YES;
}

#pragma mark - Actions

- (IBAction)didTapDone:(id)sender
{
	if ([self.textField_portNumber.text length] == 0)
	{
		[[[UIAlertView alloc] initWithTitle:@"Error" message:@"The port number can't be empty. Please set a value and try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
		return;
	}

	NSString *displayName = self.textField_portName.text;

	if (displayName == nil || [displayName length] == 0)
	{
		displayName = self.textField_portNumber.text;
	}

	NSDictionary *dict = @{kBPServerPortNameKey: displayName,
						   kBPServerPortNumberKey: self.textField_portNumber.text};

	[self.callerViewController setEditedPort:dict];

	[self.navigationController popViewControllerAnimated:YES];
}


@end

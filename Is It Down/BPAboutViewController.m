//
//  UIAboutTableViewController.m
//  Is It Down
//
//  Created by Bruno Philipe on 9/20/14.
//  Copyright (c) 2014 Bruno Philipe. All rights reserved.
//

#import "BPAboutViewController.h"
#import "BPAppDelegate.h"

#import "globalstrings.h"

#define BUNDLE_VERSION [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]
#define BUNDLE_BUILD_VERSION [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString*)kCFBundleVersionKey]

@interface BPAboutViewController ()

@property (strong, nonatomic) IBOutlet UILabel *versionLabel;
@property (strong, nonatomic) IBOutlet UISegmentedControl *portDisplayModeSegmentedControl;
@property (strong, nonatomic) IBOutlet UISwitch *switchDisplaysReloadButton;

@property (weak) NSUserDefaults *defaults;

@end

@implementation BPAboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
	[self.versionLabel setText:[NSString stringWithFormat:@"Version: %@ (%@)", BUNDLE_VERSION, BUNDLE_BUILD_VERSION]];

	_defaults = [BPAppDelegateRef sharedUserDefaults];

	NSInteger portDisplayMode = [_defaults integerForKey:kBPPortDisplayModeKey];
	[self.portDisplayModeSegmentedControl setSelectedSegmentIndex:portDisplayMode];
	
	BOOL showsReloadButton = [_defaults boolForKey:kBPShowsReloadButtonKey];
	[self.switchDisplaysReloadButton setOn:showsReloadButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == 1 && indexPath.row == 1) {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://brunophilipe.com/software/isitdown"]];
	}
}

#pragma mark - Actions

- (IBAction)portDisplayModeChanged:(UISegmentedControl*)sender
{
	[_defaults setInteger:[sender selectedSegmentIndex] forKey:kBPPortDisplayModeKey];
	[_defaults synchronize];
}

- (IBAction)displayReloadSwitchChanged:(UISwitch*)sender
{
	[_defaults setBool:[sender isOn] forKey:kBPShowsReloadButtonKey];
	[_defaults synchronize];
}

@end

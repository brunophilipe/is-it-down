//
//  UIAboutTableViewController.h
//  Is It Down
//
//  Created by Bruno Philipe on 9/20/14.
//  Copyright (c) 2014 Bruno Philipe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BPAboutViewController : UITableViewController

@end

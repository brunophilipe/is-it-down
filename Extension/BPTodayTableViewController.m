//
//  BPTodayTableViewController.m
//  Is It Down
//
//  Created by Bruno Philipe on 9/19/14.
//  Copyright (c) 2014 Bruno Philipe. All rights reserved.
//

#import <NotificationCenter/NotificationCenter.h>

#import "BPTodayTableViewController.h"
#import "BPDowntimeManager.h"
#import "BPStatusTableViewCell.h"

#import "globalstrings.h"

#define kSharedEmptyBlock ^(NCUpdateResult result){}

@interface BPTodayTableViewController () <NCWidgetProviding, BPDowntimeManagerDelegate>

@property (strong) NSArray *servers;
@property (strong) NSUserDefaults *userDefaults;
@property (strong) BPDowntimeManager *downtimeManager;
@property (strong) NSDate *lastCheckTimestamp;

@property BOOL noServers, updateFinished, showsReloadButton, systemVersion10;

@property CGFloat preferedWidth;
@property CGSize preferedSize;

@end

@implementation BPTodayTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

	self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
	self.tableView.rowHeight = 44.0;

	_preferedWidth = [[UIScreen mainScreen] bounds].size.width - 14.0;
	_systemVersion10 = [self.class getSystemVersionAsAnInteger] >= 100000;

	if ([self systemVersion10])
	{
		[self.tableView setSeparatorColor:[UIColor grayColor]];
	}

	if ([[self extensionContext] respondsToSelector:@selector(setWidgetLargestAvailableDisplayMode:)])
	{
		[[self extensionContext] setWidgetLargestAvailableDisplayMode:NCWidgetDisplayModeExpanded];
	}

	[self updatePreferedContentSize];

	self.downtimeManager = [BPDowntimeManager new];
	[self.downtimeManager setDelegate:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString*)stringForDate:(NSDate*)date
{
	NSDateFormatter* df = [NSDateFormatter new];
	[df setDateStyle:NSDateFormatterMediumStyle];
	[df setTimeStyle:NSDateFormatterMediumStyle];
	return [df stringFromDate:date];
}

- (BOOL)isCompact
{
	BOOL isCompact = NO;

	if ([[self extensionContext] respondsToSelector:@selector(widgetActiveDisplayMode)])
	{
		isCompact = [[self extensionContext] widgetActiveDisplayMode] == NCWidgetDisplayModeCompact;
	}

	return isCompact;
}

#pragma mark - Widget Providing

- (void)widgetPerformUpdateWithCompletionHandler:(void (^)(NCUpdateResult))completionHandler
{
	NSLog(@"Started loading today extension...");

	self.userDefaults = [[NSUserDefaults alloc] initWithSuiteName:kBPSharedSuiteName];

	self.showsReloadButton = [self.userDefaults boolForKey:kBPShowsReloadButtonKey];
	self.lastCheckTimestamp = [self.userDefaults objectForKey:kBPLastCheckTimestampKey];
	self.servers = [self updatedServersFromOldVersion:[self.userDefaults objectForKey:kBPServersKey]];
	
	[self.downtimeManager setServers:self.servers];
	
	if (completionHandler) completionHandler(NCUpdateResultNoData);
	
	dispatch_async(dispatch_get_main_queue(), ^
	{
		[self.downtimeManager updateServersStatus];
		
		[self.tableView reloadData];
		[self updatePreferedContentSize];
		
		NSLog(@"Finished loading today extension...");
		
		if (completionHandler) completionHandler(NCUpdateResultNewData);
	});
}

- (void)widgetActiveDisplayModeDidChange:(NCWidgetDisplayMode)activeDisplayMode withMaximumSize:(CGSize)maxSize
{
	[self.tableView reloadData];
	[self updatePreferedContentSize];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
	[coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
		self.tableView.frame = CGRectMake(0, 0, size.width, size.height);
	} completion:nil];
}

- (void)updatePreferedContentSize
{
	_preferedSize = CGSizeMake(_preferedWidth, 0.0
							   + [self.tableView numberOfRowsInSection:0] * self.tableView.rowHeight
							   + (self.showsReloadButton ? self.tableView.rowHeight : 0.0)
							   + (_systemVersion10 ? 2.0 : -1.0)
							   + self.tableView.sectionFooterHeight);

	[self setPreferredContentSize:_preferedSize];
}

- (NSAttributedString*)stringSummaryForPorts:(NSArray*)ports
{
	UIFont *font = [UIFont boldSystemFontOfSize:12];
	NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"Ports: " attributes:@{NSFontAttributeName: font}];

	for (NSDictionary *port in ports)
	{
		NSDictionary *attributes = @{NSFontAttributeName: font, NSForegroundColorAttributeName: ([[port objectForKey:kBPServerPortReachabilityKey] intValue] == 2 ? [UIColor greenColor] : [UIColor redColor])};
		[string appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"● %d", [[port objectForKey:kBPServerPortNumberKey] intValue]] attributes:attributes]];
		[string appendAttributedString:[[NSAttributedString alloc] initWithString:@", " attributes:@{NSFontAttributeName: font}]];
	}

	[string deleteCharactersInRange:NSMakeRange(string.length-2, 2)];

	return string;
}

- (NSString*)orbForPorts:(NSArray*)ports
{
	BOOL allPortsOn = YES, allPortsOff = YES;

	for (NSDictionary *port in ports) {
		NSNumber *reachability = [port objectForKey:kBPServerPortReachabilityKey];
		if (reachability) {
			if (reachability.integerValue == 2)
				allPortsOff = NO;
			else if (reachability.integerValue == 1)
				allPortsOn = NO;
		} else {
			allPortsOn = NO;
			allPortsOff = NO;
		}
	}

	if (allPortsOn)
		return @"orb_green";
	else if (allPortsOff)
		return @"orb_red";
	else
		return @"orb_yellow";
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return self.showsReloadButton && ![self isCompact] ? 2 : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (section == 0)
	{
		NSInteger count = self.servers.count;
		
		if (count > 0) {
			self.noServers = NO;
			return [self isCompact] ? MIN(count, 2) : count;
		} else {
			self.noServers = YES;
			return 1;
		}
	}
	else
	{
		return 1;
	}
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSString *const identifierServerComplex = @"cell_server_complex";
	NSString *const identifierEmpty = _systemVersion10 ? @"cell_empty_10" : @"cell_empty";
	NSString *const identifierButton = @"cell_button";
	NSString *const identifierUpdate = _systemVersion10 ? @"cell_update_10" : @"cell_update";
	NSString *identifier;

	UIColor *labelColor = [(_systemVersion10 ? [UIColor blackColor] : [UIColor whiteColor]) colorWithAlphaComponent:0.9];

	if (indexPath.section == 0)
	{
		if (self.noServers)
		{
			identifier = identifierEmpty;
		}
		else
		{
			if (indexPath.row < self.servers.count)
				identifier = identifierServerComplex;
			else
				identifier = identifierButton;
		}
	}
	else
	{
		identifier = identifierUpdate;
	}

	BPStatusTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];

	if (indexPath.section == 0)
	{
		if (!self.noServers && indexPath.row < self.servers.count)
		{
			NSDictionary *serverInfo = [self.servers objectAtIndex:indexPath.row];
			if ([serverInfo isKindOfClass:[NSDictionary class]])
			{
				NSArray *ports = [serverInfo objectForKey:kBPServerPortsKey];
				[cell.textLabel setText:[serverInfo objectForKey:kBPServerDisplayNameKey]];
				[cell.textLabel setTextColor:labelColor];
				[cell.imageView setImage:[UIImage imageNamed:[self orbForPorts:ports]]];
				[cell setPorts:ports];
			}
		}
	}

	return cell;
}

- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
	UIColor *labelColor = [(_systemVersion10 ? [UIColor blackColor] : [UIColor whiteColor]) colorWithAlphaComponent:0.9];
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell_footer"];
	NSString *text;

	if (self.lastCheckTimestamp && section == 0)
	{
		text = [NSString stringWithFormat:@"Last Update: %@", [self stringForDate:self.lastCheckTimestamp]];
	} else {
		text = @"";
	}

	[cell.textLabel setText:text];
	[cell.textLabel setTextColor:labelColor];

	return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
	return section == 0 ? 24.0 : 0.0;
}

#pragma mark - Table view delegate

/*
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	[self widgetPerformUpdateWithCompletionHandler:kSharedEmptyBlock];
}
 */

#pragma mark - Downtime Manager Delegate

- (void)downtimeManagerFinishedUpdating:(BPDowntimeManager *)manager success:(BOOL)success
{
	self.servers = [manager servers];

	if (success) {
		NSDate *now = [NSDate new];
		self.lastCheckTimestamp = now;
		[self.userDefaults setObject:now forKey:kBPLastCheckTimestampKey];
	}

	[self.userDefaults setObject:self.servers forKey:kBPServersKey];
	[self.userDefaults synchronize];

	self.updateFinished = YES;
}

#pragma mark - IBActions

- (void)openAppAction:(id)sender
{
	[self.extensionContext openURL:[NSURL URLWithString:@"isitdown://open"] completionHandler:nil];
}

- (IBAction)updateWidgetAction:(id)sender
{
	[self widgetPerformUpdateWithCompletionHandler:kSharedEmptyBlock];
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

- (NSArray *)updatedServersFromOldVersion:(NSArray *)servers
{
	NSMutableArray *newServers = [NSMutableArray arrayWithCapacity:servers.count];
	
	for (NSDictionary *server in servers) {
		NSMutableDictionary *newServer = [server mutableCopy];
		NSArray *ports = [server objectForKey:kBPServerPortsKey];
		
		if (!ports) {
			ports = @[@{kBPServerPortNameKey: @"http",
						kBPServerPortNumberKey: @80,
						kBPServerPortReachabilityKey: [server objectForKey:kBPServerReachabilityKey]}];
			[newServer setObject:ports forKey:kBPServerPortsKey];
		}
		
		[newServers addObject:newServer];
	}
	
	return [newServers copy];
}

#pragma GCC diagnostic pop

+ (NSInteger)getSystemVersionAsAnInteger
{
	int index = 0;
	NSInteger version = 0;

	NSArray* digits = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
	NSEnumerator* enumer = [digits objectEnumerator];
	NSString* number;

	while (number = [enumer nextObject])
	{
		if (index>2)
		{
			break;
		}
		NSInteger multipler = powf(100, 2-index);
		version += [number intValue]*multipler;
		index++;
	}

	return version;
}

@end

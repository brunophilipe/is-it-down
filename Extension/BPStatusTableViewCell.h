//
//  BPStatusTableViewCell.h
//  Is It Down
//
//  Created by Bruno Philipe on 9/20/14.
//  Copyright (c) 2014 Bruno Philipe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BENTagsView.h"

@interface BPStatusTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *textLabel;
@property (strong, nonatomic) IBOutlet UILabel *detailTextLabel;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet BENTagsView *tagsView;

- (void)setPorts:(NSArray*)ports;

@end

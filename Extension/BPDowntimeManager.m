//
//  BPDowntimeManager.m
//  Is It Down
//
//  Created by Bruno Philipe on 9/19/14.
//  Copyright (c) 2014 Bruno Philipe. All rights reserved.
//

#import <netdb.h>

#import "BPDowntimeManager.h"
#import "Reachability.h"

#import "globalstrings.h"

@implementation BPDowntimeManager
{
	NSMutableArray *_servers;
}

- (void)updateServersStatus
{
	Reachability *reachability = [Reachability reachabilityForInternetConnection];
		self.hadDowntimeDuringLastCheck = NO;
		
		if ([reachability currentReachabilityStatus] != NotReachable)
		{
			NSLog(@"Updating servers...");

			NSMutableArray *updatedServers = [NSMutableArray arrayWithCapacity:_servers.count];

			for (NSDictionary *serverInfo in _servers)
			{
				NSURL *url = [NSURL URLWithString:[serverInfo objectForKey:kBPServerURLKey]];
				if (url)
				{
					NSArray *oldPorts = [serverInfo objectForKey:kBPServerPortsKey];
					NSMutableArray *newPorts = [NSMutableArray arrayWithCapacity:oldPorts.count];
					NSMutableDictionary *newServerInfo = [serverInfo mutableCopy];
					if (!oldPorts || oldPorts.count == 0) oldPorts = @[@{kBPServerPortNameKey: @"http", kBPServerPortNumberKey: @80}];

					for (NSDictionary *port in oldPorts)
					{
						NSMutableDictionary *newPort = [port mutableCopy];
						BOOL isReachable = [self socketRespondedToURL:url andPort:[port objectForKey:kBPServerPortNumberKey]];
						[newPort setObject:@(isReachable ? 2 : 1) forKey:kBPServerPortReachabilityKey];

						if (!isReachable) {
							[newPort setObject:[NSDate new] forKey:kBPServerPortLastDowntimeKey];
							self.hadDowntimeDuringLastCheck = YES;
						}

						[newPorts addObject:newPort];
					}

					[newServerInfo setObject:newPorts forKey:kBPServerPortsKey];
					[updatedServers addObject:newServerInfo];
				}
			}

			_servers = [updatedServers copy];

			if ([self.delegate respondsToSelector:@selector(downtimeManagerFinishedUpdating:success:)])
				[self.delegate downtimeManagerFinishedUpdating:self success:YES];
		} else {
			NSLog(@"No internet connection.");

			if ([self.delegate respondsToSelector:@selector(downtimeManagerFinishedUpdating:success:)])
				[self.delegate downtimeManagerFinishedUpdating:self success:NO];
		}
}

- (BOOL)isServerAccessibleWithURL:(NSURL*)url andPort:(NSNumber*)port
{
	return NO;
}

- (BOOL)socketRespondedToURL:(NSURL*)url andPort:(NSNumber*)port
{
	int sockfd;

	struct sockaddr_in serv_addr;
	struct hostent *server;

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0)
		NSLog(@"ERROR opening socket");

	NSString *host = [url host];
	if (host == nil || [host length] == 0)
		return NO;

	server = gethostbyname([host UTF8String]);
	if (server == NULL)
		return NO;

	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;

	bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);
	serv_addr.sin_port = htons(port.integerValue);

	///

	//do a nonblocking connect
	//  return -1 on a system call error, 0 on success
	//  sa - host to connect to, filled by caller
	//  sock - the socket to connect
	//  timeout - how long to wait to connect
	{
		int flags = 0, error = 0, ret = 0;
		fd_set rset, wset;
		socklen_t len = sizeof(error);
		struct timeval ts;

		ts.tv_sec = 1;
		ts.tv_usec = 0;

		//clear out descriptor sets for select
		//add socket to the descriptor sets
		FD_ZERO(&rset);
		FD_SET(sockfd, &rset);
		wset = rset;    //structure assignment ok

		//set socket nonblocking flag
		if((flags = fcntl(sockfd, F_GETFL, 0)) < 0)
			return NO;

		if(fcntl(sockfd, F_SETFL, flags | O_NONBLOCK) < 0)
			return NO;

		//initiate non-blocking connect
		if((ret = connect(sockfd,(struct sockaddr *)&serv_addr, sizeof(serv_addr))) < 0)
			if (errno != EINPROGRESS) {
				NSLog(@"Error Connect: %d", errno);
				return NO;
			}

		if(ret == 0)    //then connect succeeded right away
			goto done;

		//we are waiting for connect to complete now
		if( (ret = select(sockfd + 1, &rset, &wset, NULL, 2 ? &ts : NULL)) < 0) {
			NSLog(@"Error Select: %d", errno);
			return NO;
		}
		if(ret == 0){   //we had a timeout
			errno = ETIMEDOUT;
			return NO;
		}

		//we had a positivite return so a descriptor is ready
		if (FD_ISSET(sockfd, &rset) || FD_ISSET(sockfd, &wset)){
			if(getsockopt(sockfd, SOL_SOCKET, SO_ERROR, &error, &len) < 0)
				return NO;
		}else
			return NO;

		if(error){  //check if we had a socket error
			errno = error;
			return NO;
		}

	done:
		//put socket back in blocking mode
		if(fcntl(sockfd, F_SETFL, flags) < 0)
			return NO;

		close(sockfd);

		return YES;
	}

	/*

	struct timeval timeout;
	timeout.tv_sec = 1;
	timeout.tv_usec = 0;
	printf("%d", setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (struct timeval*)&timeout, sizeof(struct timeval)));
	printf("%d", setsockopt(sockfd, SOL_SOCKET, SO_SNDTIMEO, (struct timeval*)&timeout, sizeof(struct timeval)));

	if (connect(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr)) < 0) {
		NSLog(@"");
		return NO;
	}

	n = write(sockfd,"HELO",4);

	if (n >= 0) {
		close(sockfd);
		return YES;
	}

	close(sockfd);
	return NO;
	 */
}

#pragma mark - Accessors

- (void)setServers:(NSArray *)servers
{
	_servers = [servers mutableCopy];
}

- (NSArray*)servers
{
	return _servers;
}

@end

//
//  RoundedView.h
//  Ticker Mercado BTC
//
//  Created by Bruno Philipe on 2/24/15.
//  Copyright (c) 2015 Bruno Philipe. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE @interface RoundedView : UIView

@end

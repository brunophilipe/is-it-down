//
//  BPStatusTableViewCell.m
//  Is It Down
//
//  Created by Bruno Philipe on 9/20/14.
//  Copyright (c) 2014 Bruno Philipe. All rights reserved.
//

#import "BPStatusTableViewCell.h"
#import "globalstrings.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@implementation BPStatusTableViewCell

@synthesize textLabel, detailTextLabel, imageView;

+ (UIColor*)onTagColor
{
	static UIColor *color;
	@synchronized(self)
	{
		if (!color)
			color = UIColorFromRGB(0x2a9844);
	}
	return color;
}

+ (UIColor*)offTagColor
{
	static UIColor *color;
	@synchronized(self)
	{
		if (!color)
			color = UIColorFromRGB(0xa81212);
	}
	return color;
}

- (void)didMoveToSuperview
{
	[self.tagsView setOnColor:[BPStatusTableViewCell onTagColor]];
	[self.tagsView setOffColor:[BPStatusTableViewCell offTagColor]];
	[self.tagsView setNeutralColor:[UIColor darkGrayColor]];
	[self.tagsView setTagCornerRadius:3];
}

- (void)setPorts:(NSArray *)ports
{
	NSInteger portDisplayMode = [[[NSUserDefaults alloc] initWithSuiteName:kBPSharedSuiteName] integerForKey:kBPPortDisplayModeKey];

	ports = [ports sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
		NSNumber *number1 = [obj1 objectForKey:kBPServerPortReachabilityKey];
		NSNumber *number2 =	[obj2 objectForKey:kBPServerPortReachabilityKey];

		if (!number1 && !number2) {
			return NSOrderedSame;
		} else if (!number1) {
			return NSOrderedDescending;
		} else if (!number2) {
			return NSOrderedAscending;
		} else {
			return [number1 compare:number2];
		}
	}];

	NSMutableArray *portsTags = [NSMutableArray arrayWithCapacity:ports.count];
	NSMutableIndexSet *onTags = [NSMutableIndexSet indexSet];
	NSMutableIndexSet *neutralTags = [NSMutableIndexSet indexSet];

	for (NSInteger i = 0; i < ports.count; i++) {
		NSDictionary *port = [ports objectAtIndex:i];

		id displayObject;

		switch (portDisplayMode)
		{
			case kBPPortDisplayNumber:
				displayObject = [NSString stringWithFormat:@"%d", [[port objectForKey:kBPServerPortNumberKey] intValue]];
				break;

			case kBPPortDisplayName:
				displayObject = [port objectForKey:kBPServerPortNameKey];
				break;
		}

		[portsTags addObject:displayObject];
		NSNumber *reachability = [port objectForKey:kBPServerPortReachabilityKey];
		if (!reachability)
			[neutralTags addIndex:i];
		else if ([reachability integerValue] == 2)
			[onTags addIndex:i];
	}

	[self.tagsView setNeutralIndexes:neutralTags];
	[self.tagsView setOnIndexes:onTags];
	[self.tagsView setTagStrings:[portsTags copy]];
}

@end

//
//  RoundedView.m
//  Ticker Mercado BTC
//
//  Created by Bruno Philipe on 2/24/15.
//  Copyright (c) 2015 Bruno Philipe. All rights reserved.
//

#import "RoundedView.h"

@interface RoundedView ()

@property (strong) UIColor *localBackgroundColor;

@end

@implementation RoundedView

- (id)init
{
	self = [super init];
	if (self)
	{
		[self config];
	}
	return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self)
	{
		[self config];
	}
	return self;
}

- (id)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self)
	{
		[self config];
	}
	return self;
}

- (void)config
{
	[super setBackgroundColor:[UIColor clearColor]];
}

- (void)layoutSubviews
{
	[super layoutSubviews];
	[self setNeedsDisplay];
}

- (void)setBackgroundColor:(UIColor *)backgroundColor
{
	[self setLocalBackgroundColor:backgroundColor];
}

- (UIColor*)backgroundColor
{
	return [self localBackgroundColor];
}

- (void)drawRect:(CGRect)rect {
    // Drawing code
	[[self backgroundColor] set];
	[[UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:8.0] fill];
}

@end

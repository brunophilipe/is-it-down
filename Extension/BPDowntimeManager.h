//
//  BPDowntimeManager.h
//  Is It Down
//
//  Created by Bruno Philipe on 9/19/14.
//  Copyright (c) 2014 Bruno Philipe. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BPDowntimeManager;

@protocol BPDowntimeManagerDelegate <NSObject>

- (void)downtimeManagerFinishedUpdating:(BPDowntimeManager*)manager success:(BOOL)success;

@end

@interface BPDowntimeManager : NSObject

@property (weak) id<BPDowntimeManagerDelegate> delegate;
@property (strong) NSArray *servers;

@property BOOL hadDowntimeDuringLastCheck;

- (void)updateServersStatus;

@end

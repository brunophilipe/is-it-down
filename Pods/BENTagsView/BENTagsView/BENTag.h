//
//  BENTag.h
//  BENTagsView
//
//  Created by Ben Packard on 3/13/14.
//  Copyright (c) 2014 Ben Packard. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(short, BENTagState)
{
	kBENTagStateNeutral = 1,
	kBENTagStateOff,
	kBENTagStateOn,
};

@interface BENTag : UIView

@property UILabel *textLabel;
@property BENTagState state;
@property UIColor *onColor, *offColor, *neutralColor;

@end

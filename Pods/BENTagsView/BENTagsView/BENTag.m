//
//  BENTag.m
//  BENTagsView
//
//  Created by Ben Packard on 3/12/14.
//
//

#import "BENTag.h"

@implementation BENTag
{
	BENTagState _state;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
	{
		//defaults
		self.state = kBENTagStateNeutral;
		self.offColor = self.neutralColor = [UIColor colorWithRed:0.79 green:0.79 blue:0.79 alpha:1.0];
		self.onColor = [UIColor blackColor];
				
		//add the label
		self.textLabel = [[UILabel alloc] init];
		self.textLabel.translatesAutoresizingMaskIntoConstraints = NO;
		self.textLabel.textColor = [UIColor whiteColor];
		self.textLabel.font = [UIFont systemFontOfSize:9];
		[self addSubview:self.textLabel];
		NSDictionary *views = @{@"label":self.textLabel};
		[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-7-[label]-7-|"
																	 options:0
																	 metrics:nil
																	   views:views]];
		[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-2-[label]-2-|"
																	 options:0
																	 metrics:nil
																	   views:views]];

		//refresh
		[self refresh];
    }
    return self;
}

- (void)refresh
{
	UIColor *color;

	switch (self.state)
	{
		case kBENTagStateNeutral:
			color = self.neutralColor;
			break;

		case kBENTagStateOn:
			color = self.onColor;
			break;

		case kBENTagStateOff:
			color = self.offColor;
			break;
	}

	[self setPersistantBackgroundColor:color];
}

- (BENTagState)state
{
	return _state;
}

- (void)setState:(BENTagState)state
{
	_state = state;
	[self refresh];
}

//if embedded in a UITableViewCell, on cell selection the default behavior is for all of the cell's subviews to set their background color to transparent. See SO 7053340. To avoid this, make the default implementation of setBackground do nothing, and instead provide a different method to change the color internally.
- (void)setPersistantBackgroundColor:(UIColor *)color
{
	[super setBackgroundColor:color];
}

- (void)setBackgroundColor:(UIColor *)backgroundColor
{
	//do nothing
}

@end
